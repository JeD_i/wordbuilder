**WordBuilder** is a software aimed for speech therapists. It is currently working in French only.

----------

**WordBuilder** ou **Bâtisseur de mots** est un programme dédié aux orthophonistes et aux logopèdes. Son utilisation est gratuite.

Il offre deux modes de fonctionnement :

- Construction de mots : les syllabes apparaissent dans un ordre aléatoire, l'utilisateur a la tâche de reconstruire le mot. L'application fait usage de la synthèse vocale, non seulement une fois que la solution a été trouvée, mais aussi pour permettre au joueur d'entendre le mot qu'il construit, ce qui peut l'amener à détecter de lui-même ses erreurs.

- Mots *flash* : les mots apparaissent un bref instant, l'utilisateur doit être rapide dans sa lecture.

## Fonctionnement

### Installation

Il suffit de décompresser le fichier *zip* contenant l'application et de la lancer via l'exécutable : *WordBuilder.exe*

Deux ressources sont toutefois nécessaires:

- le *framework .NET 4.5.2* disponible sur le [site de Microsoft](http://www.microsoft.com/fr-fr/download/details.aspx?id=42643). Il est fort probable que ce framework soit déjà installé sur votre ordinateur si la machine est à jour.

- une [voix de synthèse en français](https://bitbucket.org/JeD_i/wordbuilder/downloads/RSSolo4FrenchVirginie.zip), disponible dans les téléchargements du projet. Cette étape n'est pas obligatoire si votre système d'exploitation est en français.

### Utilisation

Les listes de mots utilisées par l'application se trouve dans le répertoire __*source*__ situé au même endroit que l'exécutable. Chaque fichier (au format *txt*) désigne une liste de mots, le nom du fichier étant repris comme thème de la liste. Au sein du fichier, on trouve un mot par ligne dont les syllabes sont séparées par le symbole __/__

Extrait d'un fichier *animaux.txt*
```
cro/co/di/le
hip/po/po/ta/me
```