﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace WordProvider
{
    public class FileWordProvider : IWordProvider
    {
        private string _path;

        public string Path
        {
            get { return _path; }
            set
            {
                if ( _path != value )
                {
                    _path = value;
                    _words = null;
                }
            }
        }


        public FileWordProvider()
        { }

        public FileWordProvider( string path )
        {
            Path = path;
        }

        private void Parse()
        {
            _words = new List<string[]>();

            if ( !string.IsNullOrWhiteSpace( Path ) )
            {
                try
                {
                    var lines = File.ReadAllLines( Path );
                    _words = lines.Select( x => x.Split( '/' ) ).ToList();
                }
                catch ( Exception ) { }
            }
        }

        private List<string[]> _words;

        #region IWordProvider implementation
        /// <summary>
        /// List of words from file source, randomly sorted
        /// </summary>
        public IEnumerable<string[]> Words
        {
            get
            {
                if ( _words == null )
                    Parse();

                var rnd = new Random();
                return _words.Select( x => new { A = rnd.Next() , B = x } ).OrderBy( x => x.A ).Select( x => x.B );
            }
        }
        #endregion
    }
}
