﻿using System.ComponentModel;

namespace WordProvider
{
    public class PuzzleSyllable : INotifyPropertyChanged
    {
        private string _syllable;
        public string Syllable
        {
            get { return _syllable; }
            set
            {
                if (_syllable != value)
                {
                    _syllable = value;
                    OnPropertyChanged(nameof(Syllable));
                }
            }
        }

        public int CorrectionPosition { get; set; }
        
        private int _position;
        /// <summary>
        /// Random int used for puzzle display
        /// </summary>
        public int Position
        {
            get { return _position; }
            set
            {
                if (_position != value)
                {
                    _position = value;
                    OnPropertyChanged(nameof(Position));
                }
            }
        }

        private bool _isSelected;
        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                if (_isSelected != value)
                {
                    _isSelected = value;
                    OnPropertyChanged(nameof(IsSelected));
                }
            }
        }



        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
        #endregion
    }
}
