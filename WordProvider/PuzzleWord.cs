﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace WordProvider
{
    public class PuzzleWord : INotifyPropertyChanged
    {
        private static Random PositionGenerator = new Random();

        public string Complete
        { get; set; }

        private PuzzleSyllable[] _syllables;
        public PuzzleSyllable[] Syllables
        {
            get { return _syllables; }
            set
            {
                if ( _syllables != value )
                {
                    _syllables = value;
                    OnPropertyChanged( nameof( Syllables ) );
                }
            }
        }

        private string _built = string.Empty;
        public string BuiltWord
        {
            get { return _built; }
            set
            {
                if ( _built != value )
                {
                    _built = value;
                    OnPropertyChanged( nameof( BuiltWord ) );
                }
            }
        }

        private bool _isResolved;
        public bool IsResolved
        {
            get { return _isResolved; }
            set
            {
                if ( _isResolved != value )
                {
                    _isResolved = value;
                    OnPropertyChanged( nameof( IsResolved ) );
                }
            }
        }


        private LinkedList<string> _selected;


        public PuzzleWord( string[] elements )
        {
            Complete = string.Join( "" , elements );
            _selected = new LinkedList<string>();

            Syllables = elements.Select( ( x , i ) =>
             {
                 var ps = new PuzzleSyllable { CorrectionPosition = i , IsSelected = false , Syllable = x , Position = PositionGenerator.Next() };
                 ps.PropertyChanged += OnSyllablePropertyChanged;
                 return ps;
             } )
            .OrderBy( x => x.Position ).ToArray();

            PropertyChanged += ( sender , args ) =>
            {
                switch ( args.PropertyName )
                {
                    case nameof( BuiltWord ):
                        IsResolved = string.Compare( Complete , BuiltWord ) == 0;
                        break;
                }
            };
        }

        private void OnSyllablePropertyChanged( object sender , PropertyChangedEventArgs e )
        {
            switch ( e.PropertyName )
            {
                case nameof( PuzzleSyllable.IsSelected ):
                    var ps = (PuzzleSyllable) sender;
                    if ( ps.IsSelected )
                        _selected.AddLast( ps.Syllable );
                    else
                        _selected.Remove( ps.Syllable );

                    BuiltWord = string.Join( "" , _selected );
                    break;
            }
        }


        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged( string name )
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if ( handler != null )
            {
                handler( this , new PropertyChangedEventArgs( name ) );
            }
        }
        #endregion

        public static IEnumerable<PuzzleWord> CreatePuzzles( IEnumerable<string[]> words )
        {
            return words.Select( x => new PuzzleWord( x ) );
        }
    }
}
