﻿using System.Collections.Generic;

namespace WordProvider
{
    public interface IWordProvider
    {
        IEnumerable<string[]> Words
        { get; }
    }
}
