﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WordBuilder
{
    /// <summary>
    /// Interaction logic for SourceChooser.xaml
    /// </summary>
    public partial class SourceChooser : UserControl, INotifyPropertyChanged
    {
        private string[] _sources;
        public string[] Sources
        {
            get { return _sources; }
            set
            {
                if ( _sources != value )
                {
                    _sources = value;
                    OnPropertyChanged( nameof( Sources ) );
                }
            }
        }

        private string _source;
        public string Source
        {
            get { return _source; }
            set
            {
                if ( _source != value )
                {
                    _source = value;
                    OnPropertyChanged( nameof( Source ) );
                }
            }
        }

        public Action<SourceChooser> SelectAction
        { get; set; } = x => Window.GetWindow( x ).Close();

        public SourceChooser()
        {
            InitializeComponent();
            var files = Directory.EnumerateFiles( @".\source" , "*.txt" ).Select( x => System.IO.Path.GetFileNameWithoutExtension( x ) );
            Sources = files.ToArray();
        }

        public ICommand SelectCommand => new Command( ExecuteSelectCommand , CanExecuteSelectCommand );

        private void ExecuteSelectCommand( object obj )
        {
            SelectAction( this );
        }

        private bool CanExecuteSelectCommand( object arg )
        {
            return !string.IsNullOrWhiteSpace( Source );
        }

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged( string name )
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if ( handler != null )
            {
                handler( this , new PropertyChangedEventArgs( name ) );
            }
        }
        #endregion
    }
}
