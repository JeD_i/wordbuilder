﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using WordProvider;

namespace WordBuilder
{
    public partial class FlashWindow : Window, INotifyPropertyChanged
    {
        PuzzleWord[] _puzzles;

        private string _source;
        public string Source
        {
            get { return _source; }
            set
            {
                if ( _source != value )
                {
                    _source = value;
                    OnPropertyChanged( nameof( Source ) );
                }
            }
        }
        private int _currentIndex = 1;
        public int CurrentIndex
        {
            get { return _currentIndex; }
            set
            {
                if ( value != _currentIndex )
                {
                    _currentIndex = value;
                    OnPropertyChanged( nameof( CurrentIndex ) );
                }
            }
        }

        private int _puzzlesCount;
        public int PuzzlesCount
        {
            get { return _puzzlesCount; }
            set
            {
                if ( value != _puzzlesCount )
                {
                    _puzzlesCount = value;
                    OnPropertyChanged( nameof( PuzzlesCount ) );
                }
            }
        }

        private PuzzleWord _puzzle;
        public PuzzleWord Puzzle
        {
            get { return _puzzle; }
            set
            {
                if ( value != _puzzle )
                {
                    _puzzle = value;
                    OnPropertyChanged( nameof( Puzzle ) );
                }
            }
        }

        private bool _isAllResolved;

        public bool IsAllResolved
        {
            get { return _isAllResolved; }
            set
            {
                if ( _isAllResolved != value )
                {
                    _isAllResolved = value;
                    OnPropertyChanged( nameof( IsAllResolved ) );
                }
            }
        }

        private bool _showWord;

        public bool ShowWord
        {
            get { return _showWord; }
            set
            {
                if ( _showWord != value )
                {
                    _showWord = value;
                    OnPropertyChanged( nameof( ShowWord ) );
                }
            }
        }


        public FlashWindow()
        {
            InitializeComponent();

            _sourceChooser.SelectAction = x => { Source = string.Empty; Source = x.Source; };


            PropertyChanged += ( sender , args ) =>
            {
                switch ( args.PropertyName )
                {
                    case nameof( Source ):
                        if ( !string.IsNullOrWhiteSpace( Source ) )
                        {
                            var wprd = new FileWordProvider( string.Format( "source/{0}.txt" , Source ) );
                            _puzzles = PuzzleWord.CreatePuzzles( wprd.Words ).Where( x => x.Syllables.Length <= Configuration.Instance.MaxSyllables )
                                                                           .Take( Configuration.Instance.ListLength ).ToArray();
                            PuzzlesCount = _puzzles.Length;
                            CurrentIndex = 1;

                            if ( PuzzlesCount > 0 )
                                Puzzle = _puzzles[CurrentIndex - 1];

                            CheckComplete();
                        }
                        else
                        {
                            _puzzles = null;
                            PuzzlesCount = 0;
                            Puzzle = null;
                        }
                        break;

                    case nameof( Puzzle ):
                        ShowWord = true;
                        Task.Factory.StartNew( async () =>
                        {
                            foreach ( var syl in Puzzle.Syllables.OrderBy( x => x.CorrectionPosition ) )
                            {
                                Puzzle.BuiltWord += syl.Syllable;
                                Puzzle.IsResolved = false;
                                await Task.Delay( (int) ( 1000 * Configuration.Instance.FlashSyllableTime ) );
                            }

                            Puzzle.BuiltWord = Puzzle.Complete;
                            Puzzle.IsResolved = false;
                            await Task.Delay( (int) ( 1000 * Configuration.Instance.FlashDisplayTime ) );
                            ShowWord = false;
                            await Task.Delay( (int) ( 1000 * Configuration.Instance.FlashDisplayTime ) );
                            Puzzle.IsResolved = true;
                            await Task.Delay( 1 );
                            CommandManager.InvalidateRequerySuggested();
                            CheckComplete();
                        } );
                        break;
                }
            };
        }

        private void CheckComplete()
        {
            IsAllResolved = CurrentIndex >= PuzzlesCount;
        }

        #region Commands
        public ICommand NextWordCommand => new Command( ExecuteNextWordCommand , CanExecuteNextWordCommand );
        private void ExecuteNextWordCommand( object obj )
        {
            Puzzle = _puzzles[++CurrentIndex - 1];
        }

        private bool CanExecuteNextWordCommand( object arg )
        {
            return Puzzle != null && CurrentIndex < _puzzles.Length;
        }

        public ICommand CloseWindowCommand => new Command( ExecuteCloseWindowCommand );
        private void ExecuteCloseWindowCommand( object obj )
        {
            Close();
        }

        public ICommand SayWordCommand => new Command( ExecuteSayWordCommand , CanExecuteSayWordCommand );

        private void ExecuteSayWordCommand( object obj )
        {
            AppVoice.Instance.Say(Puzzle.Complete);
        }

        private bool CanExecuteSayWordCommand( object arg )
        {
            return true;
        }
        #endregion

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged( string name )
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if ( handler != null )
            {
                handler( this , new PropertyChangedEventArgs( name ) );
            }
        }
        #endregion
    }
}
