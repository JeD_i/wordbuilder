﻿using System.Globalization;
using System.Linq;
using System.Speech.Synthesis;

namespace WordBuilder
{
    sealed class AppVoice
    {
        internal static AppVoice Instance { get; set; } = new AppVoice();

        SpeechSynthesizer _synth;

        private AppVoice()
        {
            _synth = new SpeechSynthesizer();
            ConfigureDefaultVoice(_synth);
        }

        private void ConfigureDefaultVoice(SpeechSynthesizer synth)
        {
            var frenchVoice = synth.GetInstalledVoices(CultureInfo.GetCultureInfo("fr-FR")).FirstOrDefault();
            if (frenchVoice != null)
                synth.SelectVoice(frenchVoice.VoiceInfo.Name);

            synth.Rate = -2;
        }

        public void Say(string text)
        {
            _synth.SpeakAsyncCancelAll();
            _synth.SpeakAsync(text);
        }
    }
}
