﻿using System.Windows;
using System.Windows.Input;

namespace WordBuilder
{
    public partial class MainWindow : Window
    {
        static MainWindow()
        {
            LoadSettings();
        }

        public MainWindow()
        {
            InitializeComponent();
        }

        #region Commands
        public ICommand StartGameCommand => new Command( ExecuteStartGameCommand );

        private void ExecuteStartGameCommand( object obj )
        {
            var sChr = new SourceChooser();
            var window = new Window { Content = sChr , Owner = this , WindowStartupLocation = WindowStartupLocation.CenterOwner , ResizeMode = ResizeMode.NoResize , SizeToContent = SizeToContent.WidthAndHeight };
            window.SourceInitialized += ( sender , args ) => IconHelper.RemoveIcon( window );
            window.ShowDialog();

            if ( !string.IsNullOrWhiteSpace( sChr.Source ) )
                new GameWindow { Owner = this , Source = sChr.Source }.ShowDialog();
        }

        public ICommand FlashGameCommand => new Command( ExecuteFlashGameCommand );

        private void ExecuteFlashGameCommand( object obj )
        {
            var sChr = new SourceChooser();
            var window = new Window { Content = sChr , Owner = this , WindowStartupLocation = WindowStartupLocation.CenterOwner , ResizeMode = ResizeMode.NoResize , SizeToContent = SizeToContent.WidthAndHeight };
            window.SourceInitialized += ( sender , args ) => IconHelper.RemoveIcon( window );
            window.ShowDialog();

            if ( !string.IsNullOrWhiteSpace( sChr.Source ) )
                new FlashWindow { Owner = this , Source = sChr.Source }.ShowDialog();
        }

        public ICommand ShowOptionsCommand => new Command( ExecuteShowOptionsCommand );

        private void ExecuteShowOptionsCommand( object obj )
        {
            var window = new Window
            {
                Content = new OptionsPane() ,
                Title = "Options" ,
                Owner = this ,
                WindowStartupLocation = WindowStartupLocation.CenterOwner ,
                ResizeMode = ResizeMode.NoResize ,
                SizeToContent = SizeToContent.WidthAndHeight
            };
            window.SourceInitialized += ( sender , args ) => IconHelper.RemoveIcon( window );
            window.ShowDialog();
        }

        public ICommand ShowAboutCommand => new Command( ExecuteShowAboutCommand );
        private void ExecuteShowAboutCommand( object obj )
        {
            var window = new Window
            {
                Content = new AboutPane() ,
                Title = "A propos" ,
                Owner = this ,
                WindowStartupLocation = WindowStartupLocation.CenterOwner ,
                ResizeMode = ResizeMode.NoResize ,
                SizeToContent = SizeToContent.WidthAndHeight
            };
            window.SourceInitialized += ( sender , args ) => IconHelper.RemoveIcon( window );
            window.ShowDialog();
        }
        #endregion

        private void MetroWindow_Closing( object sender , System.ComponentModel.CancelEventArgs e )
        {
            SaveSettings();
        }

        private static void SaveSettings()
        {
            Properties.Settings.Default.ListLength = Configuration.Instance.ListLength;
            Properties.Settings.Default.MaxSyllables = Configuration.Instance.MaxSyllables;
            Properties.Settings.Default.FlashDisplayTime = Configuration.Instance.FlashDisplayTime;
            Properties.Settings.Default.FlashSyllableTime = Configuration.Instance.FlashSyllableTime;

            Properties.Settings.Default.Save();
        }

        private static void LoadSettings()
        {
            Properties.Settings.Default.Reload();

            Configuration.Instance.ListLength = Properties.Settings.Default.ListLength;
            Configuration.Instance.MaxSyllables = Properties.Settings.Default.MaxSyllables;
            Configuration.Instance.FlashDisplayTime = Properties.Settings.Default.FlashDisplayTime;
            Configuration.Instance.FlashSyllableTime = Properties.Settings.Default.FlashSyllableTime;
        }
    }
}
