﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using WordProvider;

namespace WordBuilder
{
    /// <summary>
    /// Interaction logic for GameWindow.xaml
    /// </summary>
    public partial class GameWindow : Window, INotifyPropertyChanged
    {
        PuzzleWord[] _puzzles;

        private int _currentIndex = 1;
        public int CurrentIndex
        {
            get { return _currentIndex; }
            set
            {
                if ( value != _currentIndex )
                {
                    _currentIndex = value;
                    OnPropertyChanged( nameof( CurrentIndex ) );
                }
            }
        }

        private int _skipCount;
        public int SkipCount
        {
            get { return _skipCount; }
            set
            {
                if ( _skipCount != value )
                {
                    _skipCount = value;
                    OnPropertyChanged( nameof( SkipCount ) );
                }
            }
        }


        private int _score;
        public int Score
        {
            get { return _score; }
            set
            {
                if ( _score != value )
                {
                    _score = value;
                    OnPropertyChanged( nameof( Score ) );
                }
            }
        }

        private int _scorePercent;
        public int ScorePercent
        {
            get { return _scorePercent; }
            set
            {
                if ( _scorePercent != value )
                {
                    _scorePercent = value;
                    OnPropertyChanged( nameof( ScorePercent ) );
                }
            }
        }


        private int _puzzlesCount;
        public int PuzzlesCount
        {
            get { return _puzzlesCount; }
            set
            {
                if ( value != _puzzlesCount )
                {
                    _puzzlesCount = value;
                    OnPropertyChanged( nameof( PuzzlesCount ) );
                }
            }
        }

        private PuzzleWord _puzzle;
        public PuzzleWord Puzzle
        {
            get { return _puzzle; }
            set
            {
                if ( value != _puzzle )
                {
                    if ( _puzzle != null )
                        _puzzle.PropertyChanged -= Puzzle_PropertyChanged;

                    _puzzle = value;

                    if ( _puzzle != null )
                        _puzzle.PropertyChanged += Puzzle_PropertyChanged;

                    OnPropertyChanged( nameof( Puzzle ) );
                }
            }
        }

        private string _built = string.Empty;
        public string Current
        {
            get { return _built; }
            set
            {
                if ( _built != value )
                {
                    _built = value;
                    OnPropertyChanged( nameof( Current ) );
                }
            }
        }

        private string _source;

        public string Source
        {
            get { return _source; }
            set
            {
                if ( _source != value )
                {
                    _source = value;
                    OnPropertyChanged( nameof( Source ) );
                }
            }
        }

        private bool _isAllResolved;

        public bool IsAllResolved
        {
            get { return _isAllResolved; }
            set
            {
                if ( _isAllResolved != value )
                {
                    _isAllResolved = value;
                    OnPropertyChanged( nameof( IsAllResolved ) );
                }
            }
        }


        public GameWindow()
        {
            InitializeComponent();

            _sourceChooser.SelectAction = x => { Source = string.Empty; Source = x.Source; };

            PropertyChanged += ( sender , args ) =>
            {
                switch ( args.PropertyName )
                {
                    case nameof( Source ):

                        if ( !string.IsNullOrWhiteSpace( Source ) )
                        {
                            var wprd = new FileWordProvider( string.Format( "source/{0}.txt" , Source ) );
                            _puzzles = PuzzleWord.CreatePuzzles( wprd.Words ).Where( x => x.Syllables.Length <= Configuration.Instance.MaxSyllables )
                                                                           .Take( Configuration.Instance.ListLength ).ToArray();
                            PuzzlesCount = _puzzles.Length;
                            CurrentIndex = 1;
                            SkipCount = 0;

                            if ( PuzzlesCount > 0 )
                                Puzzle = _puzzles[CurrentIndex - 1];

                            CheckComplete();
                        }
                        else
                        {
                            _puzzles = null;
                            PuzzlesCount = 0;
                            Puzzle = null;
                        }

                        break;

                    case nameof( CurrentIndex ):
                        Score = CurrentIndex - 1 - SkipCount;
                        break;

                    case nameof( IsAllResolved ):
                    case nameof( SkipCount ):
                        if ( IsAllResolved )
                            Score = CurrentIndex - SkipCount;
                        break;

                    case nameof( PuzzlesCount ):
                    case nameof( Score ):
                        ScorePercent = PuzzlesCount != 0 ? (int) Math.Round( (decimal) ( Score * 100 / PuzzlesCount )  ) : 0;
                        break;
                }
            };
        }

        private void Puzzle_PropertyChanged( object sender , PropertyChangedEventArgs e )
        {
            switch ( e.PropertyName )
            {
                case nameof( PuzzleWord.IsResolved ):
                    var pw = (PuzzleWord) sender;
                    if ( pw.IsResolved )
                        AppVoice.Instance.Say( pw.Complete );
                    CheckComplete();
                    break;
            }
        }

        private void CheckComplete()
        {
            IsAllResolved = _puzzles.All( x => x.IsResolved );
        }

        #region Commands
        public ICommand CloseWindowCommand => new Command( ExecuteCloseWindowCommand );

        private void ExecuteCloseWindowCommand( object obj )
        {
            Close();
        }

        public ICommand NextWordCommand => new Command( ExecuteNextWordCommand , CanExecuteNextWordCommand );

        private void ExecuteNextWordCommand( object obj )
        {
            Puzzle = _puzzles[++CurrentIndex - 1];
        }

        private bool CanExecuteNextWordCommand( object arg )
        {
            return Puzzle != null && Puzzle.IsResolved && CurrentIndex < _puzzles.Length;
        }

        public ICommand SayWordCommand => new Command( ExecuteSayWordCommand , CanExecuteSayWordCommand );

        private bool CanExecuteSayWordCommand( object arg )
        {
            return Puzzle != null && !string.IsNullOrWhiteSpace( Puzzle.BuiltWord );
        }

        private void ExecuteSayWordCommand( object obj )
        {
            AppVoice.Instance.Say( Puzzle.BuiltWord );
        }

        public ICommand UnselectAllCommand => new Command( ExecuteUnselectAllCommand , CanExecuteUnselectAllCommand );

        private void ExecuteUnselectAllCommand( object obj )
        {
            foreach ( var syl in Puzzle.Syllables )
                syl.IsSelected = false;
        }

        private bool CanExecuteUnselectAllCommand( object arg )
        {
            return Puzzle != null && Puzzle.Syllables.Any( x => x.IsSelected );
        }

        public ICommand SolveCommand => new Command( ExecuteSolveCommand , CanExecuteSolveCommand );

        private void ExecuteSolveCommand( object obj )
        {
            foreach ( var syl in Puzzle.Syllables )
                syl.IsSelected = false;

            foreach ( var syl in Puzzle.Syllables.OrderBy( x => x.CorrectionPosition ) )
                syl.IsSelected = true;

            SkipCount++;
        }

        private bool CanExecuteSolveCommand( object arg )
        {
            return Puzzle != null && !Puzzle.IsResolved;
        }
        #endregion

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged( string name )
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if ( handler != null )
            {
                handler( this , new PropertyChangedEventArgs( name ) );
            }
        }
        #endregion

        private void ToggleButton_Checked( object sender , RoutedEventArgs e )
        {
            var tb = e.OriginalSource as ToggleButton;

            if ( tb != null && tb.IsChecked.Value )
            {
                //AppVoice.Instance.Say(tb.Content.ToString());
            }
        }
    }
}
