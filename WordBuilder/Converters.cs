﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace WordBuilder
{
    [ValueConversion( typeof( bool ) , typeof( bool ) )]
    public class BooleanInverterConverter : IValueConverter
    {
        public object Convert( object value , Type targetType , object parameter , CultureInfo culture )
        {
            return !(bool) value;
        }

        public object ConvertBack( object value , Type targetType , object parameter , CultureInfo culture )
        {
            return !(bool) value;
        }
    }

    [ValueConversion( typeof( int ) , typeof( Visibility ) )]
    public class ScoreVisibilityConverter : IValueConverter
    {
        public object Convert( object value , Type targetType , object parameter , CultureInfo culture )
        {
            var scorePercent = (int) value;
            switch ( parameter.ToString() )
            {
                case "Confused":
                    return scorePercent < 50 ? Visibility.Visible : Visibility.Collapsed;
                case "Wink":
                    return scorePercent < 100 && scorePercent >= 50 ? Visibility.Visible : Visibility.Collapsed;
                case "Smile":
                    return scorePercent == 100 ? Visibility.Visible : Visibility.Collapsed;

                default:
                    return Visibility.Collapsed;
            }
        }

        public object ConvertBack( object value , Type targetType , object parameter , CultureInfo culture )
        {
            return DependencyProperty.UnsetValue;
        }
    }

    [ValueConversion( typeof( int ) , typeof( string ) )]
    public class PluralConverter : IValueConverter
    {
        public object Convert( object value , Type targetType , object parameter , CultureInfo culture )
        {
            return (int) value <= 1 ? "mot" : "mots";
        }

        public object ConvertBack( object value , Type targetType , object parameter , CultureInfo culture )
        {
            return DependencyProperty.UnsetValue;
        }
    }
}
