﻿using System.ComponentModel;

namespace WordBuilder
{
    public sealed class Configuration : INotifyPropertyChanged
    {
        public static Configuration Instance
        { get; set; } = new Configuration();

        private int _listLength = 10;
        public int ListLength
        {
            get { return _listLength; }
            set
            {
                if ( _listLength != value )
                {
                    _listLength = value;
                    OnPropertyChanged( nameof( ListLength ) );
                }
            }
        }

        private int _maxSyllables = 5;
        public int MaxSyllables
        {
            get { return _maxSyllables; }
            set
            {
                if ( _maxSyllables != value )
                {
                    _maxSyllables = value;
                    OnPropertyChanged( nameof( MaxSyllables ) );
                }
            }
        }

        private double _flashDisplayTime = 2d;

        public double FlashDisplayTime
        {
            get { return _flashDisplayTime; }
            set
            {
                if ( _flashDisplayTime != value )
                {
                    _flashDisplayTime = value;
                    OnPropertyChanged( nameof( FlashDisplayTime ) );
                }
            }
        }


        private double _flashSyllableTime = 0d;

        public double FlashSyllableTime
        {
            get { return _flashSyllableTime; }
            set
            {
                if ( _flashSyllableTime != value )
                {
                    _flashSyllableTime = value;
                    OnPropertyChanged( nameof( FlashSyllableTime ) );
                }
            }
        }



        private Configuration()
        { }

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged( string name )
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if ( handler != null )
            {
                handler( this , new PropertyChangedEventArgs( name ) );
            }
        }
        #endregion
    }
}
