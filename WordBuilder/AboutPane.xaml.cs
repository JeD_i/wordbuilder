﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WordBuilder
{
    /// <summary>
    /// Interaction logic for AboutPane.xaml
    /// </summary>
    public partial class AboutPane : UserControl
    {
        public AboutPane()
        {
            InitializeComponent();
        }

        private void Hyperlink_RequestNavigate( object sender , RequestNavigateEventArgs e )
        {
            Process.Start( e.Uri.AbsoluteUri );
            e.Handled = true;
        }
    }

    public class AboutIconItem
    {
        public string Description
        { get; set; }

        public string Uri
        { get; set; }

        public ControlTemplate IconTemplate
        { get; set; }
    }
}
